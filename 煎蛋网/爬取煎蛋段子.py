import requests
from fake_useragent import UserAgent
from pyquery import PyQuery as pq
import csv
import time
import random
ua=UserAgent()
headers={
    'User-Agent':ua.random
}

def request_html(url):
    try:
        response=requests.get(url=url,headers=headers).text
        return response
    except Exception:
        print('request error')
data=[]
def fetch_information(html):
    doc=pq(html)
    Information=doc('.row').items()
    for i in Information:
        name=i.find('.author').text()
        text=i.find('.text p').text()
        data.append([name,text])
    return data

#保存为CSV格式
def wiert_csv(data):
    with open('v2ex.csv', 'w',encoding='gb18030') as f:
        writer = csv.writer(f)
        writer.writerow(['用户名，评论内容'])
        for row in data:
            writer.writerow(row)
			
			
print('正在保存当中 请稍后....')
start=time.time()
for i in range(1,73):
    url='http://jandan.net/duan/page-{}#comments'.format(i)
    wiert_csv(fetch_information(request_html(url)))
    time.sleep(random.randint(0,5))
print('保存成功 请到本地查看')
print('耗时:',time.time()-start)